shapes.line.addEventListener('click', () => {
    canvasFront.style.cursor = "move";
    currentTool.classList.remove('size-active');
    textOn = false;
    let position, startX, startY;

    canvasFront.onmousedown = () => {
        lineOn = true;
        position = ctxb.getImageData(0, 0 , canvasFront.width, canvasFront.height);
        startX = mouseX;
        startY = mouseY;
    };

    canvasFront.onmousemove = () => {
        if(lineOn){
            ctxb.putImageData(position,0,0);
            ctxb.beginPath();
            ctxb.moveTo(startX,startY);
            ctxb.lineTo(mouseX,mouseY);
            ctxb.stroke();
            ctxb.closePath();
        }
    };
})

shapeImg.addEventListener('click', (e) => {
    canvasFront.style.cursor = "move";
    currentTool.classList.remove('size-active');
    textOn = false;

    if (e.target.id === 'fill-rect' || e.target.id === 'rect') {
        let position, startX, startY, rectWidth, rectHeight;
        canvasFront.onmousedown = function() {
            rectOn = true;
            position = ctxb.getImageData(0, 0 , canvasFront.width, canvasFront.height);
            startX = mouseX;
            startY = mouseY;
        }

        canvasFront.onmousemove = () => {
            if (rectOn) {
                ctxb.putImageData(position,0,0);
                rectWidth = mouseX - startX;
                rectHeight = mouseY - startY;
                ctxb.strokeRect(startX, startY, rectWidth, rectHeight);
                if (e.target.id === 'fill-rect') {
                    ctxb.fillRect(startX, startY, rectWidth, rectHeight);
                }
            }
        }
    }

    if (e.target.id === 'fill-circle' || e.target.id === 'circle') {
        let position, startX, startY;

        canvasFront.onmousedown = () => {
            circleOn = true;
            position = ctxb.getImageData(0, 0 , canvasFront.width, canvasFront.height);
            startX = mouseX;
            startY = mouseY;
        }

        canvasFront.onmousemove = () => {
            if (circleOn){
                ctxb.putImageData(position,0,0);
                ctxb.beginPath();
                ctxb.arc(Math.abs(startX+mouseX)/2,Math.abs(startY+mouseY)/2,Math.sqrt(Math.pow(mouseX-startX,2)+Math.pow(mouseY-startY,2))/2,0,Math.PI*2,true);
                ctxb.closePath();
                ctxb.stroke();
                if(e.target.id === 'fill-circle'){
                    ctxb.fill();
                }
            }
        }
    }
})