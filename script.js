//							____________________CANVASES_____________________
const canvasFront = document.getElementById('front-canvas'),
	canvasBack = document.getElementById('back-canvas'),
	ctxf = canvasFront.getContext('2d'),
	ctxb = canvasBack.getContext('2d'),
	canvasWidth = document.getElementById('canvas-width'),
	canvasHeight = document.getElementById('canvas-height'),
	canvasClear = document.getElementById('clear-canvas'),
	toolsPanel = document.getElementById('tools'),
	shapeImg = document.getElementById('shapes-images');

let canvasPosition, mouseX, mouseY,
	mouseXl = document.getElementById('mouseX'),
	mouseYl = document.getElementById('mouseY');

//							____________________TOOL'S PANEL_____________________
const tools = {
	pencil: document.getElementById('pencil'),
	eraser: document.getElementById('eraser'),
	fill: document.getElementById('fill'),
	spray: document.getElementById('spray'),
	pen: document.getElementById('pen'),
	marker: document.getElementById('marker'),
	crayon: document.getElementById('crayon'),
	confetti: document.getElementById('confetti')
},
sizes = {
	small: document.getElementById('small'),
	middle: document.getElementById('middle'),
	big: document.getElementById('big')
}, 
brushes = {
	butt: document.getElementById('butt'),
	square: document.getElementById('square'),
	round: document.getElementById('round')
}, 
shapes = {
	line: document.getElementById('line'),
	rect: document.getElementById('rect'),
	fillRect: document.getElementById('fill-rect'),
	circle: document.getElementById('circle'),
	fillCircle: document.getElementById('fill-circle')
},
	undoButton = document.getElementById('undo'),
    redoButton = document.getElementById('redo'),
    currentTool = document.getElementById('current-tool'),
    toolImages = document.getElementById('tool-images'),
    chosenTool = document.getElementById('chosen-tool');
let eraserSize = 8;

// 							____________________IMAGE MANIPULATION_____________________
const fileImg = document.getElementById('img-file'), 
	properties = document.getElementById('properties'),
	imgWidth = document.getElementById('img-width'),
	imgHeight = document.getElementById('img-height'),
	saveButton = document.getElementById('save'),
	invert = document.getElementById('invert');

// 							____________________COLOR AND TEXT_____________________
const color = document.getElementById('color'),
	chosenColor = document.getElementById('chosen-color'),
	textImage = document.getElementById('text-image'),
	textSize = document.getElementById('text-size'),
    inputColor = document.getElementById('input-color'),
    hideScreen = document.getElementById('hide-screen'),
    colorButton = document.getElementById('color-button');

let spans = color.querySelectorAll('span');
for (let i = 0; i < spans.length; i++) {
	spans[i].style.background = spans[i].className;
}
//							____________________STATES_____________________
let img,
	textOn = false,
    pencilOn = false,
    eraserOn = false,
    sprayOn = false,
    lineOn = false,
    rectOn = false,
    circleOn = false,
    penOn = false,
	markerOn = false,
	crayonOn = false,
    confettiOn = false;

//    								 _______FINISH EVENTS_______
canvasFront.addEventListener('mouseup', () => {
	pencilOn = false;
	eraserOn = false;
	sprayOn = false;
    penOn = false;
    markerOn = false;
    crayonOn = false;
    confettiOn = false;
	lineOn = false;
	rectOn = false;
	circleOn = false;
    cPush();
});

canvasFront.addEventListener('mouseleave', () => {
    pencilOn = false;
	eraserOn = false;
	sprayOn = false;
    penOn = false;
    markerOn = false;
    crayonOn = false;
    confettiOn = false;
	lineOn = false;
	rectOn = false;
	circleOn = false;
    brushOn = false;
});

window.addEventListener('load', () => {
	canvasPosition = canvasBack.getBoundingClientRect();
});

canvasWidth.addEventListener('change', () => {
	canvasFront.width = canvasWidth.value;
	canvasBack.width = canvasWidth.value;
});

canvasHeight.addEventListener('change', () => {
	canvasFront.height = canvasHeight.value;
	canvasBack.height = canvasHeight.value;
});

canvasFront.addEventListener('mousemove', (e) => {
	mouseX = e.clientX - canvasPosition.left;
	mouseY = e.clientY - canvasPosition.top;
	mouseXl.innerHTML = mouseX;
	mouseYl.innerHTML = mouseY; 
});

canvasClear.addEventListener('click', () => {
    console.log(canvasWidth, canvasHeight);
	ctxf.clearRect(0, 0, canvasFront.width, canvasFront.height);
	ctxb.clearRect(0, 0, canvasBack.width, canvasBack.height);
});

//								__________ACTIVE TOOL functions__________
addAllHandlers(tools, "tool-active");
addAllHandlers(sizes, "size-active");
addAllHandlers(brushes, "size-active");
addAllHandlers(shapes, "shapes-active");

toolsPanel.addEventListener('click', (e) => {
    if (e.target.tagName === 'IMG') {
        removeAllClasses(shapes);
        textImage.style.border = '1px solid transparent';
    }
});
shapeImg.addEventListener('click', (e) => {
    if (e.target.tagName === 'IMG') {
        removeAllClasses(tools);
        textImage.style.border = '1px solid transparent';
    }
});

function addAllHandlers(obj, className) {  
	for (let item in obj) {
		obj[item].addEventListener('mousedown', addHandler(obj[item], obj, className))
	}
}

function addHandler (element, obj, className) {
	return function() {
		removeAllClasses(obj);
		element.setAttribute('class', className);
	}
}

function removeAllClasses(obj) {
	for (let item in obj) {
		obj[item].removeAttribute('class');
	}
}

document.addEventListener('click', (e) => {
    if ((e.target === currentTool || e.target.id === 'chosen-tool' || e.target.id === 'tools-box') && toolImages.style.display !== 'block') {
        toolImages.style.display = 'block';
    } else if ((e.target === currentTool || e.target.id === 'chosen-tool' || e.target.id === 'tools-box') && toolImages.style.display === 'block') {
        toolImages.style.display = 'none';
	} else {
        toolImages.style.display = 'none';
	}
});





