//                           ________PENCIL________
tools.pencil.addEventListener('click', () => {
    textOn = false;
    canvasFront.style.cursor = "crosshair";
    chosenTool.src = tools.pencil.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');

    canvasFront.onmousedown = () => {
        pencilOn = true;
        ctxb.beginPath();
        ctxb.lineTo(mouseX, mouseY);
        ctxb.stroke();
    };

    canvasFront.onmousemove = () => {
        if (pencilOn) {
            ctxb.lineTo(mouseX, mouseY);
            ctxb.stroke();
        }
    }
});
//                           ________ERASER________
tools.eraser.addEventListener('click', () => {
    canvasFront.style.cursor = 'default';
    textOn = false;
    chosenTool.src = tools.eraser.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');

    canvasFront.onmousedown = () => {
        eraserOn = true;
    };

    canvasFront.onmousemove = () => {
        if (eraserOn) {
            ctxb.clearRect(mouseX - eraserSize / 2, mouseY - eraserSize / 2, eraserSize, eraserSize);
        }
    }
});
//                           ________SPRAY________

let clientX, clientY, timeout,
    density = 50;

function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}

tools.spray.addEventListener('click', () => {
    canvasFront.style.cursor = "default";
    textOn = false;
    chosenTool.src = tools.spray.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');

    canvasFront.onmousedown = () => {
        sprayOn = true;
        clientX = mouseX;
        clientY = mouseY;
        timeout = setTimeout(function draw() {
            for (let i = density; i--; ) {
                let angle = getRandomFloat(0, Math.PI*2),
                    radius = getRandomFloat(0, 5);
                if (ctxb.lineWidth === 5) {
                    radius = getRandomFloat(0, 13);
                } else if (ctxb.lineWidth === 15) {
                    radius = getRandomFloat(0, 20);
                }

                ctxb.fillRect(clientX + radius * Math.cos(angle), clientY + radius * Math.sin(angle), 1, 1);
            }
            if (!timeout) return;
            timeout = setTimeout(draw, 30);
        }, 10);
    };

    canvasFront.onmousemove = () => {
        if(sprayOn) {
            clientX = mouseX;
            clientY = mouseY;
        }
    };
    canvasFront.addEventListener('mouseup', () => clearTimeout(timeout))
});

//                           ________PEN________
tools.pen.addEventListener('click', () => {
    textOn = false;
    chosenTool.src = tools.pen.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');

    let lastPoint;

    canvasFront.onmousedown = () =>{
        penOn = true;
        if (ctxb.lineWidth === 1) {
            ctxb.lineWidth = 1.9;
        } else if (ctxb.lineWidth === 15) {
            ctxb.lineWidth = 8;
        }
        ctxb.beginPath();
        lastPoint = { x: mouseX, y: mouseY };
    };

    canvasFront.onmousemove = () => {
        if (!penOn) return;

        ctxb.globalAlpha = 1;
        ctxb.moveTo(lastPoint.x, lastPoint.y);
        ctxb.lineTo(mouseX, mouseY);
        ctxb.stroke();

        ctxb.moveTo(lastPoint.x - 4, lastPoint.y - 4);
        ctxb.lineTo(mouseX - 4, mouseY - 4);
        ctxb.stroke();

        ctxb.moveTo(lastPoint.x - 2, lastPoint.y - 2);
        ctxb.lineTo(mouseX - 2, mouseY - 2);
        ctxb.stroke();

        ctxb.moveTo(lastPoint.x + 2, lastPoint.y + 2);
        ctxb.lineTo(mouseX + 2, mouseY + 2);
        ctxb.stroke();

        ctxb.moveTo(lastPoint.x + 4, lastPoint.y + 4);
        ctxb.lineTo(mouseX + 4, mouseY + 4);
        ctxb.stroke();

        lastPoint = { x: mouseX, y: mouseY };
    };
});

//                           ________MARKER________
tools.marker.addEventListener('click', () => {
    textOn = false;
    chosenTool.src = tools.marker.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');

    canvasFront.onmousedown = () => {
        markerOn = true;
        if (!chosenColor.style.background) {
            ctxb.shadowColor = 'rgb(0, 0, 0)';
        }
        ctxb.shadowBlur = 10;
        ctxb.shadowColor = chosenColor.style.background;
        ctxb.beginPath();
    };

    canvasFront.onmousemove = () => {
        if (!markerOn) return;
        ctxb.lineTo(mouseX, mouseY);
        ctxb.stroke();
    };

    canvasFront.addEventListener('mouseup', () => {
        ctxb.shadowBlur = 0;
    })
});

//                           ________CRAYON________
tools.crayon.addEventListener('click', () => {
    textOn = false;
    chosenTool.src = tools.crayon.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    ctxb.strokeStyle = chosenColor.style.background;
    var lastPoint;

    canvasFront.onmousedown = () => {
        crayonOn = true;
        lastPoint = {x: mouseX, y: mouseY};
    };

    canvasFront.onmousemove = () => {
        if (!crayonOn) return;

        ctxb.beginPath();

        ctxb.moveTo(lastPoint.x - getRandomInt(0, 2), lastPoint.y - getRandomInt(0, 2));
        ctxb.lineTo(mouseX - getRandomInt(0, 2), mouseY - getRandomInt(0, 2));
        ctxb.stroke();

        ctxb.moveTo(lastPoint.x, lastPoint.y);
        ctxb.lineTo(mouseX, mouseY);
        ctxb.stroke();

        ctxb.moveTo(lastPoint.x + getRandomInt(0, 2), lastPoint.y + getRandomInt(0, 2));
        ctxb.lineTo(mouseX + getRandomInt(0, 2), mouseY + getRandomInt(0, 2));
        ctxb.stroke();

        lastPoint = {x: mouseX, y: mouseY};
    };
});

//                           ________CONFETTI________
tools.confetti.addEventListener('click', () => {
    let lastPoint;
    textOn = false;
    chosenTool.src = tools.confetti.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');

    function drawPixels(x, y) {
        for (let i = -10; i < 10; i += 4) {
            for (let j = -10; j < 10; j += 4) {
                if (Math.random() > 0.5) {
                    ctxb.fillStyle = ['red', 'orange', 'yellow', 'green',
                        'light-blue', 'blue', 'purple'][getRandomInt(0, 6)];
                    ctxb.fillRect(x + i, y + j, ctxb.lineWidth, ctxb.lineWidth);
                    if (ctxb.lineWidth === 1) {
                        ctxb.fillRect(x + i, y + j, ctxb.lineWidth + 2, ctxb.lineWidth + 2);
                    }
                }
            }
        }
    }
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    canvasFront.onmousedown = () => {
        confettiOn = true;
        lastPoint = {x: mouseX, y: mouseY};
    };
    canvasFront.onmousemove = () => {
        if (confettiOn) {
            drawPixels(mouseX, mouseY);
            lastPoint = {x: mouseX, y: mouseY};
        }
    };
});



