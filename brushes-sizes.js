sizes.small.addEventListener('click', () => {
    ctxb.lineWidth = 1;
    eraserSize = 8;
});

sizes.middle.addEventListener('click', () => {
    ctxb.lineWidth = 5;
    eraserSize = 16;
});

sizes.big.addEventListener('click', () => {
    ctxb.lineWidth = 15;
    eraserSize = 32;
});

brushes.butt.addEventListener('click', () => {
    ctxb.lineCap = 'butt';
})

brushes.square.addEventListener('click', () => {
    ctxb.lineCap = 'square';
})

brushes.round.addEventListener('click', () => {
    ctxb.lineCap = 'round';
})