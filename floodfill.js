//                                          _________________FLOODFILL_________________
tools.fill.addEventListener('click', () => {
    canvasFront.style.cursor = 'pointer';
    chosenTool.src = tools.fill.src;
    toolImages.style.display = 'none';
    currentTool.classList.add('size-active');

    canvasFront.onmousedown = () => {
        floodFill(mouseX, mouseY);
        cPush();
    }
});

function floodFill(x, y){
    let borderColorRGB = chosenColor.style.background;
    borderColorRGB = borderColorRGB.replace(/[^\d,]/g, '').split(',').map(Number);
    borderColorRGB.push(255); // alpha is always 255

    let width = canvasBack.width,
        height = canvasBack.height,
        imageData = ctxb.getImageData(0, 0, width, height),
        stack = [[x, y]],
        pixel,
        originalData = ctxb.getImageData(0, 0, width, height);

    while (stack.length > 0) {
        pixel = stack.pop();
        if (pixel[0] < 0 || pixel[0] >= width) {
            continue;
        }
        if (pixel[1] < 0 || pixel[1] >= height) {
            continue;
        }

        let pointColorRGB = getColorForCoord(imageData, pixel[0], pixel[1], width); // If it's target color
        let targetColor = compareColors(pointColorRGB, borderColorRGB);

        const initialColorZone = getColorForCoord(originalData, x, y, width);  // initial color which was when we clicked
        let matchColor = compareColors(pointColorRGB, initialColorZone);

        if (targetColor === false && matchColor === true) {
            // Floodfiil this
            imageData = updateColorInImageData(imageData, pixel[0], pixel[1], width, borderColorRGB);
            // Put neighbors at stock to check
            stack.push([
                pixel[0] - 1,
                pixel[1]
            ]);
            stack.push([
                pixel[0] + 1,
                pixel[1]
            ]);
            stack.push([
                pixel[0],
                pixel[1] - 1
            ]);
            stack.push([
                pixel[0],
                pixel[1] + 1
            ]);
        }
    }
    ctxb.putImageData(imageData, 0, 0);
}

const compareColors = (color1, color2) => {
    let threshold = 2;
    redChannel = Math.abs(color1[0] - color2[0]) < threshold;
    greenChannel = Math.abs(color1[1] - color2[1]) < threshold;
    blueChannel = Math.abs(color1[2] - color2[2]) < threshold;
    alphaChannel = Math.abs(color1[3] - color2[3]) < threshold;

    return redChannel && greenChannel && blueChannel && alphaChannel;
};

const getColorForCoord = (imageData, x, y, width) => {
    const red = (y * width + x) * 4;    //y * (width * 4) + x * 4;

    return [imageData.data[red],
        imageData.data[red + 1],
        imageData.data[red + 2],
        imageData.data[red + 3]];
};

const updateColorInImageData = (imageData, x, y, width, color) => {
    const red = (y * width + x) * 4;

    imageData.data[red] = color[0];
    imageData.data[red + 1] = color[1];
    imageData.data[red + 2] = color[2];
    imageData.data[red + 3] = color[3];

    return imageData;
};
