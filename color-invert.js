color.addEventListener('change', (e) => {
    if (e.target.tagName === 'INPUT') {
        chosenColor.style.background = e.target.value;
        ctxb.fillStyle = e.target.value;
        ctxb.strokeStyle = e.target.value;
    }
})

inputColor.addEventListener('change', (e) => {
    chosenColor.style.background = e.target.value;
    ctxb.fillStyle = e.target.value;
    ctxb.strokeStyle = e.target.value;
})

document.addEventListener('click', (e) => {
    if (e.target === colorButton && hideScreen.style.visibility !== 'visible') {
        hideScreen.style.visibility = 'visible';
    } else if(e.target === colorButton && hideScreen.style.visibility === 'visible') {
        hideScreen.style.visibility = 'hidden';
    } else if (e.target.tagName === 'SPAN') {
        ctxb.strokeStyle = e.target.className;
        ctxb.fillStyle = e.target.className;
        chosenColor.style.background = e.target.id;
        chosenColor.style.background = e.target.className;
        hideScreen.style.visibility = 'hidden';
    } else {
        hideScreen.style.visibility = 'hidden';
    }
})

window.addEventListener('mousemove', () => {
    if (window.innerWidth > 1100) {
        hideScreen.style.visibility = 'hidden';
    }
})

invert.addEventListener('click', () => {
    let imageData = ctxb.getImageData(3, 3, canvasFront.width, canvasFront.height);
    for (let i = 0; i < imageData.data.length; i += 4) {
        for (let j = i; j < i + 3; j++) {
            imageData.data[j] = 255 - imageData.data[j]
        }
    }
    ctxb.putImageData(imageData, 3, 3);
})
