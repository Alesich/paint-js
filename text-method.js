textSize.addEventListener('click', () => {
    textOn = false;
    textImage.style.border = '1px solid transparent';
});

textImage.addEventListener('click', () => {
    removeAllClasses(tools);
    removeAllClasses(shapes);
    canvasFront.style.cursor = "text";
    textImage.style.border = '1px solid blue';
    textOn = true;
    currentTool.classList.remove('size-active');
    let cursorX, cursorY,
        startingX = 0,
        recentWords = [],
        undoList = [];

    function saveState() {
        undoList.push(canvasBack.toDataURL());
    }

    saveState();	//By default save the canvas state first
    cPush();

    function undo() {
        cUndo();
        undoList.pop();
        let imgData = undoList[undoList.length - 1];
        let image = new Image();

        image.src = imgData;	// Display old saved state

        image.addEventListener('load', () => {
            ctxb.clearRect(0, 0, canvasBack.width, canvasBack.height);
            ctxb.drawImage(image, 0, 0, canvasBack.width, canvasBack.height, 0, 0, canvasBack.width, canvasBack.height);
        })
    }

    canvasFront.onmousedown = () => {
        cursorX = mouseX;
        cursorY = mouseY;
        startingX = cursorX;
        recentWords = [];		// restart recent words array;
        return false;
    };

    document.onkeydown = (e) => {
        if (textOn) {
            if (!textSize.value) {
                ctxb.font = '16px Arial'
            }
            ctxb.font = textSize.value + 'px' + ' Arial';
            ctxb.fillStyle = chosenColor.style.background;

            if (e.keyCode === 8) {		// Backspace
                undo();
                let recentWord = recentWords[recentWords.length - 1];	// remove recent word
                cursorX -= ctxb.measureText(recentWord).width;
                recentWords.pop();
            } else if (e.keyCode === 13) {		// Enter
                cursorX = startingX;
                cursorY += (+textSize.value + 4);	// font-size
                if (!textSize.value) {
                    cursorY += 16;
                }
            } else if (e.keyCode === 32) {
                cursorX += +textSize.value / 2;
                if (!textSize.value) {
                    cursorX += 16 / 2;
                }
            } else if ((e.keyCode > 47 && e.keyCode < 91) || (e.keyCode > 185 && e.keyCode < 223)) {
                ctxb.fillText(e.key, cursorX, cursorY);
                cursorX += ctxb.measureText(e.key).width;
                saveState();
                recentWords.push(e.key);
            }
            cPush();
        }
    };
});