const cPushArray = [];
let cStep = -1;

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) {
        cPushArray.length = cStep;
    }
    cPushArray.push(canvasBack.toDataURL());
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        let canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function() {
            ctxb.clearRect(0, 0, canvasBack.width, canvasBack.height);
            ctxb.drawImage(canvasPic, 0, 0);
        }
    }
}

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        let canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function() {
            ctxb.clearRect(0, 0, canvasBack.width, canvasBack.height);
            ctxb.drawImage(canvasPic, 0, 0);
        }
    }
}

window.addEventListener('load', cPush);
document.addEventListener('keydown', (e) => {
    if (e.keyCode === 90 && e.ctrlKey) {
        cUndo();
    } else if (e.keyCode === 89 && e.ctrlKey) {
        cRedo();
    }
});
undoButton.addEventListener('click', cUndo);
redoButton.addEventListener('click', cRedo);