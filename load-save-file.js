fileImg.addEventListener('change', () => {
    processing = true;
    let file = fileImg.files[0];
    let reader = new FileReader();
    reader.onload = function(e) {
        let dataUri = e.target.result;
        img = new Image();
        img.onload = function() {
            ctxb.strokeRect(3, 3, img.width, img.height);
            ctxb.drawImage(img, 3, 3);
        };
        cPush();
        img.src = dataUri;
        properties.style.display = 'block';
        imgWidth.value = img.width;
        imgHeight.value =  img.height;
    };
    reader.readAsDataURL(file);
});

imgWidth.addEventListener('change', changeImgSize);
imgHeight.addEventListener('change', changeImgSize);

function changeImgSize() {
    ctxb.clearRect(0, 0, canvasBack.width, canvasBack.height);
    ctxb.strokeRect(3, 3, imgWidth.value, imgHeight.value);
    ctxb.drawImage(img, 3, 3, imgWidth.value, imgHeight.value);
    cPush();
}

saveButton.addEventListener('click', () => {
    canvasBack.toBlob((blob) => {
        let a = document.createElement("a"),
            url = URL.createObjectURL(blob);
        a.href = url;
        a.download = 'paint.png';
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    });
});